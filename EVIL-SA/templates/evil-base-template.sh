##
## A base template for getting logged into the Inception VM
## and getting to be root quickly.  Minimal configuration done here.
## For additional configuration see the extended tempate file.
##

# This sets the Prompt so we know wheich Inception VM we are on (user level)
export PS1="$Custom_PROMPT $$PS1"
# PRINT: Running "cf apps" as user ubuntu and saving that in /tmp/cf.apps.ubuntu ...
cf apps | grep "\.io" > /tmp/cf.apps.ubuntu
###############################################################################
sudo su
###############################################################################
# This sets the Prompt so we know wheich Inception VM we are on (root level)
export PS1="$Custom_PROMPT $$PS1"
VMS_INSTALLER=/opt/cisco/vms-installer
cd $$VMS_INSTALLER/scripts/
source script-constants
source ../conf/apps.properties
export PRIV_KEY_FILE=`ls -rt /opt/cisco/vms-installer/tenant-*/ssh/admin-key-* | grep -v "\.pub" | tail -1`
alias sshcmd="ssh -o StrictHostKeyChecking=no -i $$PRIV_KEY_FILE"
