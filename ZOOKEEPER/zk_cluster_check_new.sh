#!/usr/bin/env bash
#echo "Bash version ${BASH_VERSION}..."

#Variables
CAT=`which cat`
CUT=`which cut`
NC=`which nc`
GREP=`which grep`
SED=`which sed`
zk_conf=/var/vcap/jobs/zookeeper/config/
zk_bin=/var/vcap/packages/zookeeper/bin/
BORDER=####################################
zk_server1=`bosh vms --dns 2>/dev/null | grep -i 0.zoo | grep -i Running | gawk '{print $10}'`
zk_server2=`bosh vms --dns 2>/dev/null | grep -i 1.zoo | grep -i Running | gawk '{print $10}'`
zk_server3=`bosh vms --dns 2>/dev/null | grep -i 2.zoo | grep -i Running | gawk '{print $10}'`
zk_port=2181

#Functions 
function error_exit
{
	echo "$1" 1>&2
	exit 1
}

#Conditional Statements
if 
[[ ! $zk_server1 ]] && [[ ! $zk_server2 ]] &&  [[ ! $zk_server3 ]]; then error_exit " ERROR  :: Variable not set"
fi
#debug section
#echo "$MODE1 - $MODE2 - $MODE3"
leadercount=0
followercount=0

for i in $zk_server1 $zk_server2 $zk_server3
do
  MODE=`echo stat | nc $i $zk_port | $GREP Mode | awk '{ print $2 }'`
#echo $MODE
#echo $leadercount  
  if [ $MODE = "leader" ]
  then
     leadercount=$((leadercount + 1))
#echo $leadercount
  elif [ $MODE = "follower" ]
  then
     followercount=$((followercount + 1))
#echo $followercount
  else
     echo "CRITICAL :: Node $i is neither a follower nor a leader"
     error_exit " ERROR  :: Something is wrong with script"
 fi
done
#echo "Leader - $leadercount"
#echo "Follower - $followercount"

if [[ (($leadercount -eq 1)) && (($followercount -eq 2)) ]]
then
   echo "NORMAL :: Zookeeper cluster all good"
else
   if [ $leadercount -gt 1 ]
   then
      echo $leadercount
      echo "CRITICAL :: Split brain, more than 1 leader in Zookeeper cluster"
   elif [ $leadercount -gt 1 ]
   then
      echo "CRITICAL :: No Leader in Zookeper cluster"
   else
      echo "CRITICAL :: Please check the zookeeper cluster"
  fi
fi
