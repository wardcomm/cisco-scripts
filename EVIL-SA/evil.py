#!/usr/bin/python
'''
    Extensible VMS Inception Login (E.V.I.L)
    Facilitates accessing various Inception VMs in VMS and gets you logged in
    with some variable, files and aliases setup and ready to use automatically
'''

import sys
import argparse
import json
import os.path
import pexpect
# from pexpect import pxssh
import signal, termios, struct, fcntl
from string import Template

BASE_TEMPLATE_FILE="templates/evil-base-template.sh"
EXT_TEMPLATE_FILE="templates/evil-extended-template.sh"
USER_TEMPLATE_FILE="templates/evil-user-template.sh"
LOGFILE="session.log"
DEBUG=False


def readArgs () :
    parser = argparse.ArgumentParser()
    parser.add_argument("-q", "--quick", action='store_true',
        help='Quick load - this skips running the extended template file to load quicker.')
    parser.add_argument("-t", "--template-file",
        help='Also load a custom template file specified by the file name given as the argument.')
    parser.add_argument("environment",
        help="The name of the environment to log into (Ex. SADev1, P0, p-5, etc.)")
    args_namespace = parser.parse_args()
    return args_namespace


def validateFieldNotBlank (envvars, fieldName) :
    if not envvars.get(fieldName) :
        return "Could not find "+fieldName+" in the envvars JSON"
    fieldValue = envvars.get(fieldName)
    if fieldValue == "" :
        return str(fieldName+" field should not be blank.  Please provide a value.")
    return ""


def processString (input) :
    if input is None :
        return ""
    else :
        print "STARTED WITH:"
        print input
        print "ENDED WITH:"
        print input.replace("\\n", "\\\\n")
        return input
        # return str(input.replace('\n', '\\n'))

def processCommand (command) :
    if DEBUG : print ("--> "+command)
    # print ("--> "+command)
    sess = global_pexpect_instance

    if command.startswith("ssh ") :
        # print "    ... skip ssh commands ..."
        return
    if command.startswith("# ") :
        # print "    ... skip comment commands ..."
        return
    if command.strip() == "" :  # Skip empty lines
        # print "    ... skip empty line commands ..."
        return

    sess.send(command + "\n")
    i=sess.expect([pexpect.TIMEOUT, "# $", "\$ $"])
    if i == 0 : # TIMEOUT
        print ("Timed out! Exiting now")
        sys.exit(1)
    if i == 1 : # Success
        pass
        # print('==> Command completed successfully (root prompt)')
    if i == 2 : # Success
        pass
        # print('==> Command completed successfully (user prompt)')
    if i > 2 :
        print('==> UNEXPECTED RETURN CODE: '+str(i))

    if DEBUG:
        print "BEFORE: "
        print sess.before
        print "MATCH: "
        print sess.match.string
        print "AFTER: "
        print sess.after
        print "--------------------------------------------------------------"

###############################################################################
# Returns error message if anything went wrong or empty string if all succeeded
###############################################################################
def validateArgs (args) :
    # print "In validateArgs I got the following passed in: "+str(args)
    # return "Error - something bad happened!"
    # return ""
    if not hasattr(args, 'environment') :
        return "There is no environment attribute in the argument provided."

    # Try to find a file to read in
    envFileName = args.environment
    if not os.path.isfile(envFileName) :
        return "There is no file called "+envFileName

    # Now load the envvars from the file
    envvars = json.load(open(envFileName, 'r'))
    if DEBUG: print "envvars is: "+str(envvars)

    if not envvars.get('Inception_VM_PEM_FILE') :
        return "Could not find Inception_VM_PEM_FILE in file "+envFileName
    Inception_VM_PEM_FILE = envvars['Inception_VM_PEM_FILE']
    if not os.path.isfile(Inception_VM_PEM_FILE) :
        return "There is no Inception_VM_PEM_FILE called "+Inception_VM_PEM_FILE


    if not envvars.get('Custom_PROMPT') :
        return "Could not find Custom_PROMPT in file "+envFileName
    Custom_PROMPT = envvars.get('Custom_PROMPT')
    if Custom_PROMPT == "" :
        return "Custom_PROMPT should not be blank.  Please provide a value."

    if not envvars.get('Inception_VM_HOST') :
        return "Could not find Inception_VM_HOST in file "+envFileName
    Inception_VM_HOST = envvars.get('Inception_VM_HOST')
    if Inception_VM_HOST == "" :
        return "Inception_VM_HOST should not be blank.  Please provide a value."

    if not envvars.get('Inception_VM_USER') :
        return "Could not find Inception_VM_USER in file "+envFileName
    Inception_VM_USER = envvars.get('Inception_VM_USER')
    if Inception_VM_USER == "" :
        return "Inception_VM_USER should not be blank.  Please provide a value."

    # If we get here, it's all good
    return ""



def fail(message) :
    print "\n\nThere was an error found: "+message
    print "\nExiting...\n\n"
    sys.exit()


def sigwinch_passthrough (sig, data):
    # Check for buggy platforms (see pexpect.setwinsize()).
    if 'TIOCGWINSZ' in dir(termios):
        TIOCGWINSZ = termios.TIOCGWINSZ
    else:
        TIOCGWINSZ = 1074295912 # assume
    s = struct.pack ("HHHH", 0, 0, 0, 0)
    a = struct.unpack ('HHHH', fcntl.ioctl(sys.stdout.fileno(), TIOCGWINSZ , s))
    global global_pexpect_instance
    global_pexpect_instance.setwinsize(a[0],a[1])



###############################################################################
## Returns (COLUMNS, ROWS)
def getTerminalSize():
    import os
    env = os.environ
    def ioctl_GWINSZ(fd):
        try:
            import fcntl, termios, struct, os
            cr = struct.unpack('hh', fcntl.ioctl(fd, termios.TIOCGWINSZ,
        '1234'))
        except:
            return
        return cr
    cr = ioctl_GWINSZ(0) or ioctl_GWINSZ(1) or ioctl_GWINSZ(2)
    if not cr:
        try:
            fd = os.open(os.ctermid(), os.O_RDONLY)
            cr = ioctl_GWINSZ(fd)
            os.close(fd)
        except:
            pass
    if not cr:
        cr = (env.get('LINES', 25), env.get('COLUMNS', 80))

        ### Use get(key[, default]) instead of a try/catch
        #try:
        #    cr = (env['LINES'], env['COLUMNS'])
        #except:
        #    cr = (25, 80)
    return int(cr[1]), int(cr[0])



def processTemplateFile (templateFileName, envvars) :
    print "Processing template file: "+templateFileName
    global global_pexpect_instance
    sess = global_pexpect_instance

    # Make sure there is a file of that name
    # Load the JSON into a variable
    # Validate the contents
    #   The PEM File is present in JSON and exists on the file system
    #   The other fields exist in JSON -- prompt, host, user, etc.
    #   If all validations pass, carry on... See inception2.py for code
    # print "The dictionary envvars has: "+str(envvars)
    filein = open(templateFileName)
    template = Template(filein.read())
    outstr = template.substitute(envvars)

    if DEBUG :
        print "Template filled ----------------------------------------->"
        print outstr
        print "----------------------------------------------------------"

    # Process the commands in the filled in tempate now
    for command in outstr.split("\n") :
        if command.startswith("PRINT: ") :
            line = command.replace("PRINT: ", "")
            print line # print immediately
        else :
            processCommand(command)



###############################################################################
def process (args) :
    if not hasattr(args, 'environment') :
        fail ("There is no environment attribute in the argument provided.")

    # Try to find a file to read in
    envFileName = args.environment
    if not os.path.isfile(envFileName) :
        fail ("There is no file called "+envFileName)

    # Now load the envvars from the file
    envvars = json.load(open(envFileName, 'r'))
    # print "envvars is: "+str(envvars)

    if not envvars.get('Inception_VM_PEM_FILE') :
        fail ("Could not find Inception_VM_PEM_FILE in file "+envFileName)
    Inception_VM_PEM_FILE = envvars['Inception_VM_PEM_FILE']
    if not os.path.isfile(Inception_VM_PEM_FILE) :
        fail ("There is no Inception_VM_PEM_FILE called "+Inception_VM_PEM_FILE)


    if not envvars.get('Custom_PROMPT') :
        fail ("Could not find Custom_PROMPT in file "+envFileName)
    Custom_PROMPT = envvars.get('Custom_PROMPT')
    if Custom_PROMPT == "" :
        fail ("Custom_PROMPT should not be blank.  Please provide a value.")

    if not envvars.get('Inception_VM_HOST') :
        fail ("Could not find Inception_VM_HOST in file "+envFileName)
    Inception_VM_HOST = envvars.get('Inception_VM_HOST')
    if Inception_VM_HOST == "" :
        fail ("Inception_VM_HOST should not be blank.  Please provide a value.")

    if not envvars.get('Inception_VM_USER') :
        fail ("Could not find Inception_VM_USER in file "+envFileName)
    Inception_VM_USER = envvars.get('Inception_VM_USER')
    if Inception_VM_USER == "" :
        fail ("Inception_VM_USER should not be blank.  Please provide a value.")

    # ## Before we actually login, let's copy some scripts if we hav anything
    # SCRIPTS_DIR="./scripts"
    # if os.path.isdir(SCRIPTS_DIR) :
    #     print "Yes, "+SCRIPTS_DIR+" is a directory"
    #     if len(os.listdir(SCRIPTS_DIR)) > 0 :
    #         print "Yes, we have something in the scripts directory!"
    #

#    sys.exit(1)

    sesslog = open(LOGFILE, "w")

    CMD = "ssh -i "+str(Inception_VM_PEM_FILE)+" -o StrictHostKeyChecking=no "+str(Inception_VM_USER)+"@"+str(Inception_VM_HOST)
    print ">>> Login Command:    "+CMD+"\n"
    sess = pexpect.spawn(CMD, timeout=30)
    # sess = pxssh.pxssh(timeout=30, logfile=open("pxssh.log", "w"))
    # sess.login(str(Inception_VM_HOST), str(Inception_VM_USER), "", ssh_key=str(Inception_VM_PEM_FILE), quiet=False)

    (COLUMNS, LINES) = getTerminalSize()
    sess.setwinsize(LINES, COLUMNS)
    sess.logfile = sesslog
    global global_pexpect_instance
    global_pexpect_instance = sess
    i=sess.expect(["ast login: .*\$ ", pexpect.TIMEOUT]) # Look for username in the prompt
    if i == 0: # SUCCESS
        print ("Logged in successfully.  Now configuring ...")
    if i == 1: # TIMEOUT
        print ("Timed out getting logged in! Exiting now...")
        sys.exit(1)

    processTemplateFile (BASE_TEMPLATE_FILE, envvars)
    if not vars(args_namespace)['quick'] :
        processTemplateFile (EXT_TEMPLATE_FILE, envvars)
    processCommand("export SSH_LOGIN_CMD='"+CMD+"'") # Do this after sudo su
    ## If the USER_TEMPLATE_FILE exists, run it also...
    if os.path.isfile(USER_TEMPLATE_FILE) :
        print "Loading user template file ("+USER_TEMPLATE_FILE+") now..."
        processTemplateFile (USER_TEMPLATE_FILE, envvars)
    # else :
    #     print "No, did not find a user template file so nothing to do..."

    ## Add custom template if specified
    # print str(args_namespace)
    # print str(vars(args_namespace))
    custom_template=vars(args_namespace).get('template_file')
    if custom_template :
        # print "custom_template is "+custom_template
        if os.path.isfile (custom_template) :
            processTemplateFile(custom_template, envvars)
        else :
            print "Could not load template file "+custom_template
    # else :
    #     print "No custom_template provided."
    # CUSTOM_TEMPLATE = envvars.get('TEMPLATE_FILE')
    # if envvars.
    # if vars(args_namespace)['TEMPLATE_FILE'] :
    #     print "Custom Template argument was given... "+str(vars(args_namespace)['TEMPLATE_FILE'])
    # else :
    #     print "No custom template to load..."

    ## Look for a user template also to run here as well if it is found

    # # When finished, flush the buffer
    # sess.flush()
    print ""
    print "Configuration complete.  You are now in control..."
    sess.send ("\n")
    i=sess.expect([pexpect.TIMEOUT, "# $", "\$ $"])
    if i == 0 : # TIMEOUT
        print ("Timed out! Exiting now")
        sys.exit(1)
    if i == 1 : # Success
        pass
        # print('==> Command completed successfully (root prompt)')
    if i == 2 : # Success
        pass
        # print('==> Command completed successfully (user prompt)')
    if i > 2 :
        print('==> UNEXPECTED RETURN CODE: '+str(i))
    if DEBUG: print (str(sess.match.string))

    sess.send("\n")

    signal.signal(signal.SIGWINCH, sigwinch_passthrough)

    # Hand over control now...
    sess.interact(chr(29))
    sesslog.close()

    # print "\n\n*** Would now be a good time to ask about saving and renaming the log file?\n"



###############################################################################
## This is where it all begins :-)
if __name__ == '__main__':
    global global_pexpect_instance
    args_namespace = readArgs()

    process(args_namespace)
