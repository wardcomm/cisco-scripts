#!/bin/bash

SA_API_USERNAME=`cat /opt/cisco/vms-installer/conf/apps.properties | grep SA_API_USERNAME | cut -d'=' -f2`
SA_API_PASSWORD=`cat /opt/cisco/vms-installer/conf/apps.properties | grep SA_API_PASSWORD | cut -d'=' -f2`
SA_API_HOST=`cf apps | grep assurance | awk '{print $6}'`

echo "SA_API_USERNAME=$SA_API_USERNAME"
echo "SA_API_PASSWORD=$SA_API_PASSWORD"
echo "SA_API_HOST=$SA_API_HOST"

echo "curl -kg -u $SA_API_USERNAME:$SA_API_PASSWORD http://$SA_API_HOST/assurance-api/v1.0/service | python -m json.tool > /tmp/saapi.txt"
curl -kg -u $SA_API_USERNAME:$SA_API_PASSWORD http://$SA_API_HOST/assurance-api/v1.0/service | python -m json.tool > /tmp/saapi.txt
cat /tmp/saapi.txt | grep -i "\"topLevelServiceID\":" | cut -d'"' -f4 | sort -u  ## lists Services from SA_API

