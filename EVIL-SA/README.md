Extensible VMS Inception Login (E.V.I.L)
=========================================
This is a Python script to automatically log a user into a VMS Inception VM and execute the initial commands to get the environment setup as desired.  The commands are stored in a template which can be enhanced to suit the users needs.

Getting Started
---------------
Getting started is simple.  Just four easy steps.
1. Collect information needed:
  * Inception VM Hostname
  * Username (usually ubuntu)
  * ssh key file (pem file)
2. Setup local files:
  * Copy the ssh key file to the pem_keys directory
  * Create a JSON file for the environment.  Use sample.json as an example.
  * You can have as many of these files as you need so feel free to setup several like p0.json, p1.json, etc.
3. Verify login works:  
  * ssh -i pem_keys/file user@host
  * Accept the key if necessary
4. Run python evil.py file.json and enjoy!
  * You can use the "-q" option to log in quickly but you will miss out on a lot of the useful stuff done for you in the extended template file if you do that.

Contribute
----------
If you have some useful aliases, exports or other commands that would help others, please send them to John Lynde (jlynde@cisco.com) for consideration to be added to the extended template file.

Having Trouble?
---------------
There is a file created each time this is run called session.log.  You can look in there for clues about what could be going wrong.

Need more help?
----------------
Contact John Lynde (jlynde@cisco.com)

How does it work?
-----------------
It's actually not all that complicated.  This script uses the information in the JSON file and the ssh key file and called pexpect (Python Expect) to log into the Inception VM and run the commands in the tempates in the templates directory.  Then it transfers control over to you at the end.  The templates can be extended and you can add your own code to create additional aliases, for example.  

Want To Extend It?
------------------
All you need to do is to create a file called evil-user-template and put it in the templates directory.  Add commands to that file and they will be run after everything else is done and just prior to giving you control.  For example, if you had the following contents:

    export EVIL_USER_TEMPLATE=Yes
    alias evil='echo "Yes, the evil user template has been processed!  Buwahahahaha!"'

When you got control you would have a new environment variable and a new alias.  See for yourself!
